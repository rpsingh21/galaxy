package promometrics

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// RunMetricServer run
func RunMetricServer() {
	log.Println("start promometrics server ------------->")
	http.Handle("/", promhttp.Handler())
	http.ListenAndServe("0:2112", nil)
	log.Println("Exit server ------------->")
}
