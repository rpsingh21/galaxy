package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/go-zookeeper/zk"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/rpsingh21/galaxy/sync-service/promometrics"
)

var (
	// opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
	// 		Name: "myapp_processed_ops_total",
	// 		Help: "The total number of processed events",
	// })
	logger       = log.New(os.Stdout, "[sync-service] ", log.LstdFlags|log.Lshortfile)
	eventCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "sync_service_meter_event_processed",
		Help: "The total number of all process records",
	})
)

// https://reqres.in/api/users?page=100
func main() {
	lockPath := "/ds/counter"
	// serviceID := fmt.Sprintf("UUID-%d", os.Getpid())
	zkHosts := []string{"127.0.0.1:2181", "127.0.0.1:2182", "127.0.0.1:2183"}
	zooCoon := createConection(zkHosts)

	lock := createLock(zooCoon, lockPath)

	go promometrics.RunMetricServer()

	for {
		if f := getNewSegment(zooCoon, lock, lockPath); !f {
			logger.Println("Failed to get segment")
			break
		}
		exicuteNewSegment()
	}
}

func createConection(zkHosts []string) *zk.Conn {
	zooCoon, _, err := zk.Connect(zkHosts, 15*time.Second)
	if err != nil {
		panic(err)
	}
	return zooCoon
}

func createLock(zooCoon *zk.Conn, path string) *zk.Lock {
	aclk := zk.WorldACL(zk.PermAll)
	lock := zk.NewLock(zooCoon, path, aclk)
	return lock
}

func getNewSegment(zooCoon *zk.Conn, lock *zk.Lock, lockPath string) bool {
	logger.Println("Watting for lock .......")
	if err := lock.Lock(); err != nil {
		panic(err)
	}
	res, flgs, err := zooCoon.Get(lockPath)
	if err != nil {
		panic(err)
	}
	counter, err := strconv.Atoi(string(res))

	fmt.Printf("current version => %v, value %v\n", flgs.Version, counter)
	zooCoon.Set(lockPath, []byte(strconv.Itoa(counter+25)), flgs.Version)
	if err != nil {
		log.Println(err)
	}
	// Todo: Remove
	if err := lock.Unlock(); err != nil {
		panic(err)
	}
	logger.Println("value update ", counter+25)
	return true
}

func exicuteNewSegment() {
	wg := sync.WaitGroup{}

	for i := 0; i < 25; i++ {
		wg.Add(1)
		url := fmt.Sprintf("https://reqres.in/api/users?page=%d", i)
		log.Println("Request start for", url)
		go apiCall(url, &wg)
	}

	wg.Wait()
}

func apiCall(url string, wg *sync.WaitGroup) {
	defer wg.Done()
	res, err := http.Get(url)
	if err != nil {
		logger.Println(err)
		return
	}
	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	eventCounter.Inc()
	if err != nil {
		log.Fatalln(err)
	}
	logger.Println(body)
}
