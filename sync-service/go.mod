module gitlab.com/rpsingh21/galaxy/sync-service

go 1.16

require (
	github.com/go-zookeeper/zk v1.0.2 // indirect
	github.com/prometheus/client_golang v1.11.0 // indirect
)
